import { Schema } from "mongoose";

export const NewsSchema = new Schema({
    created_at: {
        type: Date,
        required: true
    },
    title: String,
    url: String,
    author: {
        type:String,
        required: true
    },
    points: String,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    created_at_i: {
        type: Number,
        required: true
    },
    _tags:Array,
    objectID: {
        type:String,
        unique: true, 
        required:true
    },
    _highlightResult: Object,
    active:{
        type: Boolean,
        default: true
    }
})

NewsSchema.methods.toJSON = function(){
    const {__v, _id, active, ...news} = this.toObject()
    return news
}

import { Schema } from 'mongoose'
import { hash } from 'bcrypt'

export const UserSchema = new Schema({
    email:{
        type: String,
        unique: true,
        required: true
    },
    password:{
        type: String,
        required: true
    }
})

UserSchema.pre('save', async function (next) {
    try{
        if(!this.isModified("password")){
            return next()
        }
        const hashed = await hash(this["password"], 10)
        this["password"] = hashed
        return next()
    }catch(err){
        return next(err)
    }

})


import { Module } from '@nestjs/common';
import { ApiService } from './api.service';
import { MongooseModule } from "@nestjs/mongoose";
import { NewsSchema } from "../models/news.schema";
import { HttpModule } from "@nestjs/axios";
import { ApiController } from './api.controller';

@Module({
  imports: [MongooseModule.forFeature([
    {name: 'News', schema:NewsSchema}
  ]), HttpModule],
  providers: [ApiService],
  controllers: [ApiController]
})
export class ApiModule {}

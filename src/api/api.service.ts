import { Injectable, HttpException, HttpStatus, } from '@nestjs/common';
import { HttpService } from "@nestjs/axios";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose'
import { News } from "../interfaces/news.interface";
import { CreateNews } from "./dto/api.dto";
import { Cron } from '@nestjs/schedule';

@Injectable()
export class ApiService {
    constructor(
        @InjectModel('News') private newsModel: Model<News>,
        private httpService: HttpService,
        ){}

    @Cron('0 23 * * * *')
     async getNewsFromApi(){
        const {data} = await this.httpService.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs").toPromise()
        const hits = data.hits
        await this.newsModel.insertMany(hits,{ordered:false})
        // await this.newsModel.deleteMany({})
        return hits
    }

    
}

export class CreateNews{
    created_at: Date;
    title: string;
    url: string;
    author: string;
    points: string;
    story_text: string;
    comment_text: string;
    num_comments: number;
    story_id: number;
    story_title: string;
    story_url: string;
    parent_id: number;
    created_at_i: number;
    _tags: any;
    objectID: string;
    _highlightResult: any;
}
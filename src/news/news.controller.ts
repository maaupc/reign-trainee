import { Controller, Delete, Get, HttpStatus, NotFoundException, Param, Query, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
    constructor(private newService: NewsService){}

    @Get()
    @UseGuards(AuthGuard("jwt"))
    async getNews(@Res() res, @Query('title') title, @Query('author') author, @Query('date') date){
        if(author){
            const news = await this.newService.getNewsByAuthor(author)
            return res.status(HttpStatus.OK).json({
                news
            })
        }

        if(date){
            const news = await this.newService.getNewsByDate(date)
            return res.status(HttpStatus.OK).json({
                news
            })

        }

        if(title){
            const news = await this.newService.getNewsByTitle(title)
            return res.status(HttpStatus.OK).json({
                news
            })
        }

        const news = await this.newService.getNews()
        return res.status(HttpStatus.OK).json({
            news
        })
    }

    @Delete('/:objectID')
    @UseGuards(AuthGuard("jwt"))
    async deleteNews(@Res() res, @Param('objectID') objectID){
        const news = await this.newService.deleteNews(objectID)

        if(!news){
            throw new NotFoundException("News does not exist")
        }

        return res.status(HttpStatus.OK).json({
            message: "News deleted succesfully",
            news
        })

    }



}

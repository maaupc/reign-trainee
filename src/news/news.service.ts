import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { News } from 'src/interfaces/news.interface';
import * as moment from "moment";


@Injectable()
export class NewsService {
    constructor(@InjectModel('News') private newsModel: Model<News>){}

    async getNews(): Promise<News[]>{
        const news = await this.newsModel.find({active:true}).limit(5)
        return news
    }

    async getNewsByAuthor(author: string): Promise<News[]>{
        const news = await this.newsModel.find({active:true, author}).limit(5)
        return news
    }

    async getNewsByTitle(title: string): Promise<News[]>{
        const news = await this.newsModel.find({active:true, title}).limit(5)
        return news
    }

    async getNewsByDate(date: string){
        const news = await this.newsModel.find({active:true})

        const newsFilter = news.filter((news)=>{
            return moment().month(date).isSame(news.created_at, "month")
        })        
        return newsFilter.slice(0, 5)
    }

    async deleteNews(objectID: string){
        const news = await this.newsModel.findOneAndUpdate({objectID}, {active:false})
        return news
    }



}

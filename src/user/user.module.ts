import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "../models/user.schema";
import { UserController } from './user.controller';
import { AuthService } from 'src/auth/auth.service';

@Module({
  imports:[MongooseModule.forFeature([
    {name:'User', schema: UserSchema}
  ])],
  exports: [UserService],
  controllers:[UserController],
  providers: [UserService],
})
export class UserModule {}

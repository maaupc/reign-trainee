import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose'
import { Payload } from 'src/interfaces/auth.interface';
import { User } from "../interfaces/user.interface";
import { UserDTO } from "./dto/user.dto";

@Injectable()
export class UserService {
    constructor(@InjectModel('User') private userModel: Model<User>){}

    async createUser(userDTO: UserDTO){
        const {email} = userDTO
        const user = await this.userModel.findOne({email})

        if(user){
            throw new HttpException("User already exists", HttpStatus.BAD_REQUEST)
        }

        const createdUser = new this.userModel(userDTO)
        await createdUser.save()
        return this.onlyEmail(createdUser)

    }

    async findByEmail(user){
        const userFind = await this.userModel.findOne({email: user.email})
        return this.onlyEmail(userFind)
    }

    onlyEmail(user: User){
        const userEmail = user.toObject()
        delete userEmail['password']
        return userEmail
    }

    async findByPayload(payload: Payload){
        const {email} = payload
        return await this.userModel.findOne({email})
    }


}

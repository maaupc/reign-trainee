import { Controller, Body, Get, Post, UseGuards } from '@nestjs/common';
import { UserDTO } from 'src/user/dto/user.dto';
import { UserService } from 'src/user/user.service';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/login.dto';

@Controller('auth')
export class AuthController {
    
    constructor(
        private userService:UserService,
        private authService: AuthService
    ){}



    @Post('register')
    async registerUser(@Body() userDTO: UserDTO){
        const user = await this.userService.createUser(userDTO)
        const payload = {email: user.email}

        const token = await this.authService.signPayload(payload)
        return {
            user,
            token
        }

    }

    @Post('login')
    async loginUser(@Body() userDTO:LoginDTO ){
        const user = await this.userService.findByEmail(userDTO)
        const payload = {email: user.email}
        const token = await this.authService.signPayload(payload)

        return{
            user,
            token
        }

    }



}


## Description

API that consume a external API which shows recently posted articles about NodeJS on Hacker News. 

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

##  API Docs

### Authentication required

For all the request you will need to authenticate to have a token, wich will be sent through Authorization - Bearer Token

If you are not register:

**Register**

POST:  http://localhost:3000/auth/register

Sending email and password from the **Body**

**Authentication**

POST: http://localhost:3000/auth/login

Sending email and password from the **Body**


------------



#### Search

**Filter by author**

GET: http://localhost:3000/news?author=...

**Filter by Title**

GET: http://localhost:3000/news?title=...

**Fitler by Month**

GET: http://localhost:3000/news?date=...

*Note: The filter by month only accept words: January, February, March, April, May, June, July, August, September, October, November, December*


#### Delete 

DELETE: http://localhost:3000/news/< objectID >

To delete a new replace < objectID > with the objectID from the new you want to delete.

*Note: The deleted news will not appear when the app is restarted*


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
